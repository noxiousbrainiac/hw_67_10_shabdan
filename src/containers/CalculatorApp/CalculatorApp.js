import React from 'react';
import NumeralKey from "../../components/UI/NumeralKey/NumeralKey";
import Button from "../../components/UI/Button/Button";
import {useDispatch, useSelector} from "react-redux";

const numerals = [
    {value: 0},
    {value: 1},
    {value: 2},
    {value: 3},
    {value: 4},
    {value: 5},
    {value: 6},
    {value: 7},
    {value: 8},
    {value: 9},
    {value: "."}
];

const CalculatorApp = () => {
    const result = useSelector(state => state.result);
    const dispatch = useDispatch();

    const calculate = (e) => {
        e.preventDefault();
        dispatch({type: "EVAL"});
    };

    const handleClick = e => dispatch({type: "HANDLECLICK", payload: e.currentTarget.name});

    const handleChange = e => dispatch({type: "HANDLECHANGE", payload: e.target.value});

    const clear = ()=> dispatch({type: "CLEAR"});

    return (
        <div className="container">
            <div
                className="card p-2 bg-light"
                style={{width: "210px", marginTop: "40px"}}
            >
                <div
                    className="d-flex justify-content-end"
                >
                    <div
                        style={{width: "144px"}}
                        className="d-flex justify-content-start flex-wrap"
                    >
                        <form onSubmit={calculate}>
                            <input
                                className="form-control my-1"
                                type="text"
                                value={result}
                                onChange={handleChange}
                            />
                        </form>
                        {numerals.map(key => (
                            <NumeralKey
                                key={key.value}
                                name={key.value}
                                onClick={handleClick}
                            >
                                {key.value}
                            </NumeralKey>
                        ))}
                        <Button onClick={calculate}>=</Button>
                    </div>
                    <div className="d-flex flex-column">
                        <Button onClick={clear}>C</Button>
                        <Button name={"+"} onClick={handleClick}>+</Button>
                        <Button name={"-"} onClick={handleClick}>-</Button>
                        <Button name={"/"} onClick={handleClick}>/</Button>
                        <Button name={"*"} onClick={handleClick}>*</Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CalculatorApp;
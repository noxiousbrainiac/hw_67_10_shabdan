import React from 'react';

const Button = ({children, onClick, name}) => {
    return (
        <button
            style={{width: "40px", height: "40px"}}
            className="btn btn-warning d-block m-1"
            type={"button"}
            onClick={(e) => onClick(e)}
            name={name}
        >
            {children}
        </button>
    );
};

export default Button;
import React from 'react';

const NumeralKey = ({children,onClick, name}) => {
    return (
        <button
            style={{width: "40px", height: "40px"}}
            className="btn btn-primary m-1"
            type={"button"}
            onClick={(e) => onClick(e)}
            name={name}
        >
            {children}
        </button>
    );
};

export default NumeralKey;
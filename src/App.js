import React from 'react';
import CalculatorApp from "./containers/CalculatorApp/CalculatorApp";

const App = () => {
    return (
        <div>
          <CalculatorApp/>
        </div>
    );
};

export default App;
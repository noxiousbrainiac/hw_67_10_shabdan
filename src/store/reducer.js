const initialState = {
    result: ""
}

const reducer = (state = initialState, action) => {
    if (action.type === "EVAL") {
        try {
            return {...state, result: eval(state.result).toString()};
        } catch (Err) {
            alert("Такой операции не существует !");
            return {...state, result: ""};
        }
    }

    if (action.type === "HANDLECLICK") {
        return {...state, result:  `${state.result}${action.payload}`}
    }

    if (action.type === "HANDLECHANGE") {
        return {...state, result: `${action.payload}`}
    }

    if (action.type === "CLEAR"){
        return {...state, result: ""}
    }
    return state;
};

export default reducer;